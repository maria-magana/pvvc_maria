FROM islasgeci/base:0.7.0

RUN R -e "install.packages(c('comprehenr', 'rjson'), repos = 'http://cran.rstudio.com')"
RUN Rscript -e "install.packages(c('covr', 'devtools', 'styler', 'testthat'), repos='http://cran.rstudio.com')"