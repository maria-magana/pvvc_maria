.PHONY: clean testthat

check:
	R -e "library(styler)" \
	  -e "resumen <- style_dir('R')" \
	  -e "resumen <- rbind(resumen, style_dir('src'))" \
	  -e "resumen <- rbind(resumen, style_dir('tests'))" \
	  -e "any(resumen[[2]])" \
	  | grep FALSE

clean:
	rm --recursive --force data
	rm --recursive --force reports/*.aux
	rm --recursive --force reports/*.bbl
	rm --recursive --force reports/*.bcf
	rm --recursive --force reports/*.blg
	rm --recursive --force reports/*.csv
	rm --recursive --force reports/*.log
	rm --recursive --force reports/*.out
	rm --recursive --force reports/*.pdf
	rm --recursive --force reports/figures/*.png
	rm --recursive --force reports/tables/*
	rm --recursive --force tstex_modules
	rm --recursive --force *.pdf

data/processed/csvEstacionesMeteorologicasIslaGuadalupeEngDate.csv: data/raw/Estaciones_Meteorologicas_Isla_Guadalupe.csv
	mkdir --parents $(@D)
	Rscript src/clean_data.R

data/processed/csvEstacionesMeteorologicasIslaGuadalupeWeeklyAveragesPista.csv: data/processed/csvEstacionesMeteorologicasIslaGuadalupeEngDate.csv
	mkdir --parents $(@D)
	Rscript src/weekly_averages.R

data/raw/Estaciones_Meteorologicas_Isla_Guadalupe.csv:
	mkdir --parents $(@D)
	descarga_datos $(@F) $(@D) Estaciones_Meteorologicas_Isla_Guadalupe

data/raw/datapackage.json:
	mkdir --parents $(@D)
	descarga_datos $(@F) $(@D) Estaciones_Meteorologicas_Isla_Guadalupe

reports/figures/plot_external_temperature_time_series_Guadalupe_2008_2019.png: data/processed/csvEstacionesMeteorologicasIslaGuadalupeEngDate.csv
	mkdir --parents $(@D)
	Rscript src/time_series.R

reports/figures/plot_control_chart_Guadalupe_2008_2019.png: data/processed/csvEstacionesMeteorologicasIslaGuadalupeWeeklyAveragesPista.csv
	mkdir --parents $(@D)
	Rscript src/control_chart.R

reports/paper.pdf: reports/paper.tex
	cd reports && pdflatex paper.tex
	cd reports && bibtex paper
	cd reports && pdflatex paper.tex
	cd reports && pdflatex paper.tex

reports/tables.pdf: reports/tables.tex reports/tables/firstable.csv reports/tables/tabla_metadatos.csv reports/tables/fivenums.csv
	cd reports && pdflatex tables.tex
	cd reports && pdflatex tables.tex

reports/tables/firstable.csv: data/raw/Estaciones_Meteorologicas_Isla_Guadalupe.csv
	mkdir --parents $(@D)
	Rscript src/first_table.R

reports/tables/fivenums.csv: data/raw/Estaciones_Meteorologicas_Isla_Guadalupe.csv
	mkdir --parents $(@D)
	Rscript src/5numeros.R

reports/tables/tabla_metadatos.csv: data/raw/datapackage.json
	mkdir --parents $(@D)
	Rscript src/json2csv.R

testthat:
	Rscript -e "testthat::test_dir('tests/testthat/', report = 'summary', stop_on_failure = TRUE)"